package com.example.misaki.myapp05.model.data.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiResponse<T> {
    private ArrayList<String> productTypes;
    @SerializedName("resultCode")
    private int resultCode;
    @SerializedName("error")
    private String error;
    @SerializedName("result")
    private T result;

    public int getResultCode() {
        return resultCode;
    }

    public String getError() {
        return error;
    }

    public T getResult() {
        return result;
    }

}
