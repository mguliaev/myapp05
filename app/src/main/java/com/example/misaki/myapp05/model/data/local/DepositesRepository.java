package com.example.misaki.myapp05.model.data.local;

import com.example.misaki.myapp05.model.products.Deposit;

import java.util.List;

public class DepositesRepository {
    List<Deposit> deposits;
    private static final DepositesRepository ourInstance = new DepositesRepository();

    public static DepositesRepository getInstance() {
        return ourInstance;
    }

    private DepositesRepository() {
    }

    public List<Deposit> getDeposits() {
        return deposits;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }
}
