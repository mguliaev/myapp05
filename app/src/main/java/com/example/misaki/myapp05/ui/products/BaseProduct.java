package com.example.misaki.myapp05.ui.products;

public class BaseProduct {
    static final int BASE = 0;
    static final int TITLE = 1;
    static final int ACCOUNT = 2;
    static final int DEPOSIT = 3;
    static final int CREDIT_CARD = 4;
    static final int DEBIT_CARD = 5;
    static final int CREDIT = 7;

    protected int type = BASE;

    int getType() {
        return type;
    }
}
