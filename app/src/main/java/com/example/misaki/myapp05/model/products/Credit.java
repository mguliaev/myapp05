package com.example.misaki.myapp05.model.products;

public class Credit extends Product {

    @Override
    public String toString() {
        return name
                + " " + Double.toString(percent) + "%"
                + " " + Double.toString(amount)
                + " " + currency;
    }

}
