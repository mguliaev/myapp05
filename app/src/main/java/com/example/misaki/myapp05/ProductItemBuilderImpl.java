package com.example.misaki.myapp05;

import javax.inject.Inject;

import dagger.Provides;
import io.reactivex.annotations.NonNull;

public class ProductItemBuilderImpl implements ProductItemBuilder {

    @NonNull
    private TimeFormatter timeFormatter;

    @Inject
    public ProductItemBuilderImpl(@NonNull TimeFormatter timeFormatter) {
        this.timeFormatter = timeFormatter;
    }

}

