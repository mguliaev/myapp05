package com.example.misaki.myapp05.ui;

import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;

import com.example.misaki.myapp05.MainActivity;

public abstract class PinStrategy {
    public abstract void setTitle(TextView v);
    public abstract void setForgetButton(Button v);

    void onComplete(String pin_key, String pinValue, PinActivity pinActivity, Class<MainActivity> mainActivityClass) {
        Intent intent = new Intent(pinActivity, mainActivityClass);
        pinActivity.startActivity(intent);
        pinActivity.finish();
    }
}
