package com.example.misaki.myapp05.ui;

import android.widget.Button;
import android.widget.TextView;

import com.example.misaki.myapp05.R;

public class InputPinStrategy extends PinStrategy {
    @Override
    public void setTitle(TextView v) {
        v.setText(R.string.pin_title_input);
    }

    @Override
    public void setForgetButton(Button v) {
        v.setText(R.string.pin_forget);
    }

}
