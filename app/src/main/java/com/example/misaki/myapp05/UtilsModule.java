package com.example.misaki.myapp05;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {

    @Provides
    TimeFormatter provideTimeFormatter() {
        return new DayMonthTimeFormatter();
    }

//    @Provides
//    ProductItemBuilder provideProductItemBuilder(TimeFormatter timeFormatter) {
//        return new ProductItemBuilderImpl(timeFormatter);
//    }

}

