package com.example.misaki.myapp05.model.products;

import com.google.gson.annotations.SerializedName;

public class Product {
    @SerializedName("name")
    protected String name;
    @SerializedName("percent")
    protected double percent;
    @SerializedName("amount")
    protected double amount;
    @SerializedName("currency")
    protected String currency;
    @SerializedName("bonusPercent")
    protected double bonusPercent;
    @SerializedName("status")
    protected String status;

    public Product() {
    }

    public Product(String name, double percent, double amount, String currency, double bonusPercent, String status) {
        this.name = name;
        this.percent = percent;
        this.amount = amount;
        this.currency = currency;
        this.bonusPercent = bonusPercent;
        this.status = status;
    }

    double getPercent() {
        return percent;
    }

    double getAmount() {
        return amount;
    }

    String getCurrency() {
        return currency;
    }

    double getBonusPercent() {
        return bonusPercent;
    }

    public String getStatus() {
        return status;
    }
}
