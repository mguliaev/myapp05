package com.example.misaki.myapp05;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class BuilderModule {

    @Binds
    abstract ProductItemBuilder provideProductItemBuilder(ProductItemBuilderImpl itemBuilder);

}

