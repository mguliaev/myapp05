package com.example.misaki.myapp05.model.data.remote;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class DownloadService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        task();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void task() {
        DownloadServiceParams downloadServiceParams = new DownloadServiceParams(this,
                100000, 1000);
        MyAsyncTask myAsyncTask = new MyAsyncTask(downloadServiceParams);
        myAsyncTask.execute(downloadServiceParams);
    }
}
