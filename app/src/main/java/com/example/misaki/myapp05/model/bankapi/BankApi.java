package com.example.misaki.myapp05.model.bankapi;

import com.example.misaki.myapp05.model.data.response.ApiResponse;
import com.example.misaki.myapp05.model.data.response.ProductsResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface BankApi {
    @GET("products")
    Observable<ApiResponse<ProductsResponse>> getProducts();
}
