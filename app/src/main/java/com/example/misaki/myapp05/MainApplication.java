package com.example.misaki.myapp05;

import android.app.Application;

import io.reactivex.annotations.NonNull;

public class MainApplication extends Application {

    private AppComponent component;
    //private RepositoryComponent repositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        component = DaggerAppComponent.builder()
                .networkModule(new NetworkModule())
                .utilsModule(new UtilsModule())
                .build();

//        repositoryComponent = DaggerRepositoryComponent.builder()
//                .repositoryModule(new RepositoryModule())
//                .build();
    }

    @NonNull
    public AppComponent getComponent() {
        return component;
    }
}
