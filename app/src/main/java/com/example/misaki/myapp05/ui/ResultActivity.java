package com.example.misaki.myapp05.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.misaki.myapp05.R;
import com.example.misaki.myapp05.model.data.response.ApiResponse;
import com.example.misaki.myapp05.model.data.response.ApiResponseFromProductsResponse;
import com.example.misaki.myapp05.model.data.response.ProductsResponse;
import com.google.gson.Gson;

public class ResultActivity extends AppCompatActivity {

    private String jsonString;

    private EditText logEditText;
    private TextView statusTextView;
    private Button processResultButton;
    private Button showResultButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        ControlsFind();
        ControlsBind();

        Intent intent = getIntent();
        jsonString = intent.getStringExtra("Response");
        logEditText.setText(jsonString);

    }

    private void ControlsFind() {
        logEditText = findViewById(R.id.logEditText);
        statusTextView = findViewById(R.id.statusTextView);
        processResultButton = findViewById(R.id.processResultButton);
        showResultButton = findViewById(R.id.showResultButton);
    }

    private void ControlsBind() {
        processResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResponseProcess(jsonString);
            }
        });
        showResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResultShow(jsonString);
            }
        });
    }

    void ResponseProcess(String s) {
        Gson gson = new Gson();
        ApiResponse<ProductsResponse> apiResponse = gson.fromJson(jsonString, ApiResponseFromProductsResponse.class);
        ProductsResponse productsResponse = apiResponse.getResult();
        String slog = "rcode=" + Integer.toString(apiResponse.getResultCode())
                + " error=\"" + apiResponse.getError() + "\""
                + " asize=" + Integer.toString(productsResponse.getAccountSize());
        statusTextView.setText(slog);
    }

    void ResultShow(String s) {
        Intent intent = new Intent(this, ResultShowActivity.class);
        intent.putExtra("Response", s);
        startActivity(intent);
    }

}
