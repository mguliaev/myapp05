package com.example.misaki.myapp05.model.products;

import java.util.Comparator;

public class DepositSortByDateAmountComparator implements Comparator<Deposit> {
    @Override
    public int compare(Deposit left, Deposit right) {
        int res1 = left.getDate().compareTo(right.getDate());
        if (res1 != 0) {
            return res1;
        }
        return ((Double) left.getAmount()).compareTo((Double) right.getAmount());
    }
}
