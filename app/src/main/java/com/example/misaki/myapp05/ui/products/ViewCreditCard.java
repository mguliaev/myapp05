package com.example.misaki.myapp05.ui.products;

public class ViewCreditCard {
    private String text = "<empty>";

    public ViewCreditCard(String text) {
        this.text = text;
    }

    String getText() {
        return text;
    }
}
