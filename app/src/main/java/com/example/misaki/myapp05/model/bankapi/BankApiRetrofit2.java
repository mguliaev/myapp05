package com.example.misaki.myapp05.model.bankapi;

import com.example.misaki.myapp05.model.data.response.ApiResponse;
import com.example.misaki.myapp05.model.data.response.ProductsResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BankApiRetrofit2 {

    @GET("products")
    Call<ApiResponse<ProductsResponse>> getProducts();
}

