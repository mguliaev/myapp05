package com.example.misaki.myapp05.model.data.remote;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.example.misaki.myapp05.model.products.DepositSortByDateAmountComparator;
import com.example.misaki.myapp05.model.products.Deposit;
import com.example.misaki.myapp05.model.data.local.DepositesRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MyAsyncTask extends AsyncTask<DownloadServiceParams, Integer, List<Deposit>> {

    DownloadServiceParams downloadServiceParams;

    public MyAsyncTask(DownloadServiceParams downloadServiceParams) {
        this.downloadServiceParams = downloadServiceParams;

    }

    @Override
    protected void onPreExecute() {
        Intent intent = new Intent("custom_action");
        intent.putExtra("action_progress_max", downloadServiceParams.getNumDef());
        //downloadServiceParams.getService().sendBroadcast(intent);
        LocalBroadcastManager.getInstance(downloadServiceParams.getService()).sendBroadcast(intent);
        publishProgress(0);
    }

    @Override
    protected List<Deposit> doInBackground(DownloadServiceParams... params) {
        // Код, выполняющийся в фоновом потоке

        //downloadServiceParams = params[0];

        String[] names = {"Магнит", "Аптека", "Банк", "Айкай"};
        List<Deposit> deposits = new ArrayList<>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        publishProgress(0);

        int numSendMessage = 0;
        int numStepDef = downloadServiceParams.getNumDef() / 100;

        for (int i = 0; i < downloadServiceParams.getNumDef(); i++) {
            Deposit deposit = new Deposit();
            deposit.setName(names[i % names.length]);
            int daysBack = (int) Math.round(Math.random() * 365.0);
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, -daysBack);
            int day = c.get(Calendar.DAY_OF_MONTH);
            int month = c.get(Calendar.MONTH);
            int year = c.get(Calendar.YEAR);
            String dateStr = Integer.toString(year)
                    + "-" + Integer.toString(month)
                    + "-" + Integer.toString(day);
            Date date = null;
            try {
                date = format.parse(dateStr);
            } catch (ParseException e) {

            }
            deposit.setDate(date);
            deposit.setAmount((int) Math.round(Math.random() * 1000.0));
            deposits.add(deposit);

            if (i >= numSendMessage) {
                publishProgress(i);
                numSendMessage += numStepDef;
            }
        }

        publishProgress(downloadServiceParams.getNumDef());

        DepositesRepository.getInstance().setDeposits(deposits);
        Intent intent = new Intent("custom_action");
        intent.putExtra("action_progress_sort", downloadServiceParams.getNumDef());
        //downloadServiceParams.getService().sendBroadcast(intent);
        LocalBroadcastManager.getInstance(downloadServiceParams.getService()).sendBroadcast(intent);

        Collections.sort(deposits, new DepositSortByDateAmountComparator());
        List<Deposit> resultDeposits;
        if (deposits.size() <= downloadServiceParams.getResultNumMaxDef()) {
            resultDeposits = deposits;
        } else {
            resultDeposits = deposits.subList(0, downloadServiceParams.getResultNumMaxDef());
        }

        return resultDeposits;
    }

    @Override
    protected void onProgressUpdate(Integer... integers) {
        int value = integers[0];
        Intent intent = new Intent("custom_action");
        intent.putExtra("action_progress", value);
        //downloadServiceParams.getService().sendBroadcast(intent);
        LocalBroadcastManager.getInstance(downloadServiceParams.getService()).sendBroadcast(intent);
    }

    @Override
    protected void onPostExecute(List<Deposit> deposits) {
        DepositesRepository.getInstance().setDeposits(deposits);
        Intent intent = new Intent("custom_action");
        intent.putExtra("action_done", downloadServiceParams.getNumDef());
        //downloadServiceParams.getService().sendBroadcast(intent);
        LocalBroadcastManager.getInstance(downloadServiceParams.getService()).sendBroadcast(intent);
    }

    @Override
    protected void onCancelled(List<Deposit> deposits) {
        Intent intent = new Intent("custom_action");
        intent.putExtra("action_progress_off", downloadServiceParams.getNumDef());
        //downloadServiceParams.getService().sendBroadcast(intent);
        LocalBroadcastManager.getInstance(downloadServiceParams.getService()).sendBroadcast(intent);
    }
}
