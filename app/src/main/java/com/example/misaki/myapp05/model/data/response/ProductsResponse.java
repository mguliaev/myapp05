package com.example.misaki.myapp05.model.data.response;

import com.example.misaki.myapp05.model.products.Credit;
import com.example.misaki.myapp05.model.products.CreditCard;
import com.example.misaki.myapp05.model.products.DebitCard;
import com.example.misaki.myapp05.model.products.Deposit;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductsResponse {
    @SerializedName("accounts")
    private List<Credit> accounts;
    @SerializedName("deposits")
    private List<Deposit> deposits;
    @SerializedName("creditCards")
    private List<CreditCard> creditCards;
    @SerializedName("debitCards")
    private List<DebitCard> debitCards;
    @SerializedName("credits")
    private List<Credit> credits;

    public int getAccountSize() {
        return accounts.size();
    }

    public Credit getAccount(int index) {
        if (index >= 0 && index < accounts.size()) {
            return accounts.get(index);
        }
        return null;
    }

    public int getDepositsSize() {
        if (deposits == null) {
            return 0;
        }
        return deposits.size();
    }

    public Deposit getDeposit(int index) {
        if (deposits == null) {
            return null;
        }
        if (index >= 0 && index < deposits.size()) {
            return deposits.get(index);
        }
        return null;
    }

    public int getCreditCardsSize() {
        if (creditCards == null) {
            return 0;
        }
        return creditCards.size();
    }

    public CreditCard getCreditCard(int index) {
        if (creditCards == null) {
            return null;
        }
        if (index >= 0 && index < creditCards.size()) {
            return creditCards.get(index);
        }
        return null;
    }

    public Credit getCredit(int index) {
        if (credits == null) {
            return null;
        }
        if (index >= 0 && index < credits.size()) {
            return credits.get(index);
        }
        return null;
    }

    public int getDebitCardsSize() {
        if (debitCards == null) {
            return 0;
        }
        return debitCards.size();
    }

    public DebitCard getDebitCard(int index) {
        if (debitCards == null) {
            return null;
        }
        if (index >= 0 && index < debitCards.size()) {
            return debitCards.get(index);
        }
        return null;
    }

    public int getCreditsSize() {
        if (credits == null) {
            return 0;
        }
        return credits.size();
    }
}
