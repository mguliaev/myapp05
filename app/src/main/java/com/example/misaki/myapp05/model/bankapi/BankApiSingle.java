package com.example.misaki.myapp05.model.bankapi;

import com.example.misaki.myapp05.model.data.response.ApiResponse;
import com.example.misaki.myapp05.model.data.response.ProductsResponse;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface BankApiSingle {
    @GET("products")
    Single<ApiResponse<ProductsResponse>> getProducts();
}
