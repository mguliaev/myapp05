package com.example.misaki.myapp05;

import android.app.Activity;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class MyPreferences {
    public static void saveText(Activity activity, String key, String s) {
        SharedPreferences sPref = activity.getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(key, s);
        ed.commit();
    }

    public static String loadText(Activity activity, String key) {
        SharedPreferences sPref = activity.getPreferences(MODE_PRIVATE);
        return sPref.getString(key, "");
    }
}
