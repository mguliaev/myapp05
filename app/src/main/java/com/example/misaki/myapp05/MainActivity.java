package com.example.misaki.myapp05;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.misaki.myapp05.model.bankapi.BankApi;
import com.example.misaki.myapp05.model.bankapi.BankApiRetrofit2;
import com.example.misaki.myapp05.model.bankapi.BankApiSingle;
import com.example.misaki.myapp05.model.data.local.DepositesRepository;
import com.example.misaki.myapp05.model.data.local.LocalRepository;
import com.example.misaki.myapp05.model.products.Deposit;
import com.example.misaki.myapp05.model.products.DepositSortByDateAmountComparator;
import com.example.misaki.myapp05.model.data.remote.DownloadService;
import com.example.misaki.myapp05.model.data.response.ApiResponse;
import com.example.misaki.myapp05.model.data.response.ApiResponseFromProductsResponse;
import com.example.misaki.myapp05.model.data.response.ProductsResponse;
import com.example.misaki.myapp05.model.data.response.ResponseMock;
import com.example.misaki.myapp05.ui.PinActivity;
import com.example.misaki.myapp05.ui.products.BaseProduct;
import com.example.misaki.myapp05.ui.products.BaseProductsAdapter;
import com.example.misaki.myapp05.ui.products.ProductsConverter;
import com.example.misaki.myapp05.ui.products.ViewProductsAdapter;
import com.example.misaki.myapp05.ui.products.ViewResponseObjects;
import com.example.misaki.myapp05.ui.ProductsFragment;
import com.example.misaki.myapp05.ui.NotificationFragment;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import android.support.design.widget.BottomNavigationView;
import android.view.Menu;
import android.view.MenuItem;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {

    @Inject
    BankApi bankApi;
    @Inject
    ProductItemBuilder itemBuilder;
    @Inject
    DepositesRepository depositesRepository;
    @Inject
    LocalRepository localRepository;

    @BindView(R.id.urlEditText)
    EditText urlEditText;
    @BindView(R.id.requestButton)
    Button requestButton;
    @BindView(R.id.requestDefButton)
    Button requestDefButton;
    @BindView(R.id.progressTestButton)
    Button progressTestButton;
    @BindView(R.id.progressBarHorizontal)
    ProgressBar progressBarHorizontal;
    @BindView(R.id.serviceTestButton)
    Button serviceTestButton;
    @BindView(R.id.requestRxJavaButton)
    Button requestRxJavaButton;

    //@BindView(R.id.fragmentsBottomNavigationView)
    BottomNavigationView fragmentsBottomNavigationView;

    private int progressCount = 0;

    LooperThread looperThread;

    private static Handler mainUIHandler;

    private BroadcastReceiver broadcastListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // обработка поступивших от службы данных
            if (intent.hasExtra("action_progress_max")) {
                int numMax = intent.getIntExtra("action_progress_max", 0);
                progressBarHorizontal.setMax(numMax);
                progressBarHorizontal.setVisibility(View.VISIBLE);
            }
            if (intent.hasExtra("action_progress")) {
                int num = intent.getIntExtra("action_progress", 0);
                progressBarHorizontal.setProgress(num);
                serviceTestButton.setText("Ok:" + Integer.toString(num));
            }
            if (intent.hasExtra("action_progress_sort")) {
                int num = intent.getIntExtra("action_progress_sort", 0);
                serviceTestButton.setText("Sort:" + Integer.toString(num) + ",wait...");
            }
            if (intent.hasExtra("action_progress_off")) {
                progressBarHorizontal.setVisibility(View.INVISIBLE);
            }
            if (intent.hasExtra("action_done")) {
                int num = intent.getIntExtra("action_done", 0);
                progressBarHorizontal.setVisibility(View.INVISIBLE);
                //List<Deposit> deposits = DepositesRepository.getInstance().getDeposits();
                List<Deposit> deposits = depositesRepository.getInstance().getDeposits();
                depositsToRecyclerView(deposits);
                serviceTestButton.setText("Ok:Broadcast");
            }
        }
    };

    private LocalBroadcastManager localBroadcastManager;

    private class MyRunnable implements Runnable {
        private int value;
        private int valueMax;
        List<Deposit> deposits;

        MyRunnable(int value, int valueMax, List<Deposit> deposits) {
            this.value = value;
            this.valueMax = valueMax;
            this.deposits = deposits;
        }

        @Override
        public void run() {
            // код для исполнения в главном потоке
            progressTestButton.setText("Ok:" + Integer.toString(value));
            progressBarHorizontal.setMax(valueMax);
            progressBarHorizontal.setProgress(value);
            if (value == 0) {
                progressBarHorizontal.setVisibility(View.VISIBLE);
            } else if (value == valueMax) {
                progressBarHorizontal.setVisibility(View.INVISIBLE);
                if (deposits == null) {
                    progressTestButton.setText("Sort:" + Integer.toString(value) + ",wait...");
                } else {
                    progressTestButton.setText("Ok:" + Integer.toString(value));
                    depositsToRecyclerView(deposits);
                }
            }
        }
    }

    private class LooperThread extends Thread {

        static final int numDef = 100000;
        static final int numStepDef = numDef / 100;
        static final int resultNumMaxDef = 1000;

        Handler handler;

        public void run() {
            Looper.prepare();
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 0) {
                        String[] names = {"Магнит", "Аптека", "Банк", "Айкай"};
                        List<Deposit> deposits = new ArrayList<>();

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        Handler h = new Handler(Looper.getMainLooper());
                        Message message0 = Message.obtain(h, new MyRunnable(0, numDef, null));
                        message0.what = 0;
                        message0.sendToTarget();

                        int numSendMessage = 0;

                        for (int i = 0; i < numDef; i++) {
                            Deposit deposit = new Deposit();
                            deposit.setName(names[i % names.length]);
                            int daysBack = (int) Math.round(Math.random() * 365.0);
                            Calendar c = Calendar.getInstance();
                            c.add(Calendar.DATE, -daysBack);
                            int day = c.get(Calendar.DAY_OF_MONTH);
                            int month = c.get(Calendar.MONTH);
                            int year = c.get(Calendar.YEAR);
                            String dateStr = Integer.toString(year)
                                    + "-" + Integer.toString(month)
                                    + "-" + Integer.toString(day);
                            Date date = null;
                            try {
                                date = format.parse(dateStr);
                            } catch (ParseException e) {

                            }
                            deposit.setDate(date);
                            deposit.setAmount((int) Math.round(Math.random() * 1000.0));
                            deposits.add(deposit);

                            if (i >= numSendMessage) {
                                Message message = Message.obtain(h, new MyRunnable(i, numDef, null));
                                message.what = i;
                                message.sendToTarget();
                                numSendMessage += numStepDef;
                            }
                        }

                        Message message100 = Message.obtain(h, new MyRunnable(numDef, numDef, null));
                        message100.what = numDef;
                        message100.sendToTarget();

                        Collections.sort(deposits, new DepositSortByDateAmountComparator());
                        List<Deposit> resultDeposits;
                        if (deposits.size() <= resultNumMaxDef) {
                            resultDeposits = deposits;
                        } else {
                            resultDeposits = deposits.subList(0, resultNumMaxDef);
                        }

                        Message message101 = Message.obtain(h, new MyRunnable(numDef, numDef, resultDeposits));
                        message101.what = numDef;
                        message101.sendToTarget();
                    }
                }
            };
            Looper.loop();
        }

        Handler getHandler() {
            return handler;
        }

    }

    private FragmentManager fragmentManager;

    void Clear_BackStack() {
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        fragmentManager = getSupportFragmentManager();
//        Fragment fragment = ProductsFragment.newInstance();
//        final FragmentTransaction transaction = fragmentManager.beginTransaction();
//        transaction.replace(R.id.fragmentBase, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();

//         urlEditText = findViewById(R.id.urlEditText);
//         requestButton = findViewById(R.id.requestButton);
//         requestDefButton = findViewById(R.id.requestDefButton);
//         progressTestButton = findViewById(R.id.progressTestButton);
//         progressBarHorizontal = findViewById(R.id.progressBarHorizontal);
//         serviceTestButton = findViewById(R.id.serviceTestButton);
//         requestRxJavaButton = findViewById(R.id.requestRxJavaButton);

        fragmentsBottomNavigationView = (BottomNavigationView) findViewById(R.id.fragmentsBottomNavigationView);
        fragmentsBottomNavigationView.inflateMenu(R.menu.bottom_menu);

        fragmentsBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                int id = item.getItemId();
                switch (id) {
                    case R.id.menuProducts:
                        fragment = ProductsFragment.newInstance();
                        break;
                    case R.id.menuNotification:
                        fragment = NotificationFragment.newInstance();
                        break;
                    case R.id.menuSettings:
                        //fragment = SettingsFragment.newInstance(FragmentNum);
                        break;
                }
                if (fragment != null) {
                    Clear_BackStack();
                    final FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragmentBase, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                return true;
            }
        });

        //((MainApplication) getApplication()).getComponent().inject(this);

        MainApplication mainApplication = (MainApplication)getApplication();
        AppComponent component = mainApplication.getComponent();
        component.inject(this);

        ButterKnife.bind(this);

        looperThread = new LooperThread();
        looperThread.start();

        mainUIHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                // TODO Auto-generated method stub
                //progressTestButton.setText("Ok:" + Integer.toString(message.what));
                int v = message.what;
            }
        };

        IntentFilter intentFilter = new IntentFilter("custom_action");
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(broadcastListener, intentFilter);

        //registerReceiver(broadcastListener, intentFilter); /// !!! Через localBroadcastManager не работает!

        MyPreferences.saveText(this, PinActivity.PIN_TIME_KEY, "");
    }

    @Override
    protected void onDestroy() {
        looperThread.getHandler().getLooper().quit();
        localBroadcastManager.unregisterReceiver(broadcastListener);
        //unregisterReceiver(broadcastListener);
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Calendar c = Calendar.getInstance();
        String dateStr = Integer.toString(c.get(Calendar.YEAR))
                + "-" + String.format("%02d", c.get(Calendar.MONTH))
                + "-" + String.format("%02d", c.get(Calendar.DAY_OF_MONTH))
                + " " + String.format("%02d", c.get(Calendar.HOUR))
                + ":" + String.format("%02d", c.get(Calendar.MINUTE))
                + ":" + String.format("%02d", c.get(Calendar.SECOND));
        MyPreferences.saveText(this, PinActivity.PIN_TIME_KEY, dateStr);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        String dateStr = MyPreferences.loadText(this, PinActivity.PIN_TIME_KEY);
        if(dateStr.length() > 0) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date;
            try {
                date = format.parse(dateStr);
                Calendar c1 = Calendar.getInstance();
                Calendar c2 = Calendar.getInstance();
                //c1.setTime(date);
                c2.add(Calendar.SECOND, -30);

                Calendar c;

                c = c1;
                String dateStr1 = Integer.toString(c.get(Calendar.YEAR))
                        + "-" + String.format("%02d", c.get(Calendar.MONTH))
                        + "-" + String.format("%02d", c.get(Calendar.DAY_OF_MONTH))
                        + " " + String.format("%02d", c.get(Calendar.HOUR))
                        + ":" + String.format("%02d", c.get(Calendar.MINUTE))
                        + ":" + String.format("%02d", c.get(Calendar.SECOND));

                c = c2;
                String dateStr2 = Integer.toString(c.get(Calendar.YEAR))
                        + "-" + String.format("%02d", c.get(Calendar.MONTH))
                        + "-" + String.format("%02d", c.get(Calendar.DAY_OF_MONTH))
                        + " " + String.format("%02d", c.get(Calendar.HOUR))
                        + ":" + String.format("%02d", c.get(Calendar.MINUTE))
                        + ":" + String.format("%02d", c.get(Calendar.SECOND));

                //if (c1.compareTo(c2) > 0) {
                if(dateStr.compareTo(dateStr2) < 0) {
                    Intent intent = new Intent(this, PinActivity.class);
                    this.startActivity(intent);
                    this.finish();
                }
            } catch (ParseException e) {

            }
        }
    }

    @OnClick(R.id.requestButton)
    public void onRequestButtonClick(View v) {
        requestByRetrofit();
    }

    @OnClick(R.id.requestRxJavaButton)
    public void onRequestRxJavaButtonClick(View v) {
        requestByRxJava();
    }

    @OnClick(R.id.requestDefButton)
    public void onRequestDefButtonClick(View v) {
        urlEditText.setText(R.string.url_def);
        Gson gson = new Gson();
        ApiResponse<ProductsResponse> apiResponse = gson.fromJson(ResponseMock.responseOrg, ApiResponseFromProductsResponse.class);
        productsToRecyclerView(apiResponse);
    }

    @OnClick(R.id.progressTestButton)
    void onProgressTestButtonClick(View v) {
        progressTestButton.setText("Start");
        if (looperThread.getHandler() != null) {
            Message message = looperThread.getHandler().obtainMessage(0);
            message.sendToTarget();
        }
    }

    @OnClick(R.id.serviceTestButton)
    void onServiceTestButtonClick(View v) {
        Intent intent = new Intent(this, DownloadService.class);
        //intent.putExtra("url", "https://visa.com/card.pdf");
        startService(intent);
    }

    void requestByRetrofit() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.mocky.io/v2/5b21590730000091265c7459/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        BankApiRetrofit2 bankApi = retrofit.create(BankApiRetrofit2.class);

        Call<ApiResponse<ProductsResponse>> call = bankApi.getProducts();

        call.enqueue(new Callback<ApiResponse<ProductsResponse>>() {

            @Override
            public void onResponse(@NonNull Call<ApiResponse<ProductsResponse>> call,
                                   @NonNull Response<ApiResponse<ProductsResponse>> response) {
                ApiResponse<ProductsResponse> apiResponse = response.body();
                ProductsResponse productsResponse = apiResponse.getResult();
                productsToRecyclerView(apiResponse);
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse<ProductsResponse>> call, @NonNull Throwable t) {
                Log.e("DEXSYS", t.getMessage());
            }

        });

    }

    void requestByRxJava() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                //.baseUrl("http://www.mocky.io/v2/")
                .baseUrl("http://www.mocky.io/v2/5b21590730000091265c7459/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();


        BankApiSingle bankApi = retrofit.create(BankApiSingle.class);
        ProductsConverter converter = new ProductsConverter();

        bankApi.getProducts()
                .subscribeOn(Schedulers.io())
                .map(response -> converter.convert(response.getResult()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> baseProductsToRecyclerView(response),
                        error -> onRequestDefButtonClick(null)
                );

    }

    private void productsToRecyclerView(ApiResponse<ProductsResponse> apiResponse) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ViewResponseObjects products = new ViewResponseObjects(apiResponse);
        RecyclerView.Adapter adapter = new ViewProductsAdapter(products);
        recyclerView.setAdapter(adapter);
    }


    public void depositsToRecyclerView(List<Deposit> deposits) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ViewResponseObjects products = new ViewResponseObjects(deposits);
        RecyclerView.Adapter adapter = new ViewProductsAdapter(products);
        recyclerView.setAdapter(adapter);
    }

    public void baseProductsToRecyclerView(List<BaseProduct> baseProducts) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.Adapter adapter = new BaseProductsAdapter(baseProducts);
        recyclerView.setAdapter(adapter);
    }

}
