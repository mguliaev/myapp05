package com.example.misaki.myapp05.ui.products;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.misaki.myapp05.R;

public class ViewDebitCardAdapter extends RecyclerView.Adapter<ViewDebitCardAdapter.ViewHolder> {
    ViewDebitCards debitCards;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private View mainView;

        public ViewHolder(View v) {
            super(v);
            this.mainView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ViewDebitCardAdapter(ViewDebitCards debitCards) {
        this.debitCards = debitCards;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewDebitCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.debit_card_item_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ViewDebitCard debitCard = debitCards.getDebitCard(position);
        TextView debitCardTextView = holder.mainView.findViewById(R.id.debitCardItemTextView);
        debitCardTextView.setText(debitCard.getText());
    }

    // Return the numDef of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return debitCards.getSize();
    }

}

