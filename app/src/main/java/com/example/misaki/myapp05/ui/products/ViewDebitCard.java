package com.example.misaki.myapp05.ui.products;

public class ViewDebitCard {
    private String text = "<empty>";

    public ViewDebitCard(String text) {
        this.text = text;
    }

    String getText() {
        return text;
    }
}
