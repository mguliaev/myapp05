package com.example.misaki.myapp05.ui.products;

import java.util.ArrayList;

public class ViewCredits extends BaseProduct {

    protected ArrayList<ViewCredit> credits;

    public ViewCredits(ArrayList<ViewCredit> credits) {
        type = BaseProduct.CREDIT;
        this.credits = credits;
    }

    String getText() {
        String s;
        if(credits==null) {
            s = "<null>";
        } else {
            s = Integer.toString(credits.size());
        }
        return "ViewCredits: " + s;
    }

    int getSize() {
        if (credits == null) {
            return 0;
        }
        return credits.size();
    }

    ViewCredit getCredit(int index) {
        if (credits == null) {
            return null;
        }
        if (index >= 0 && index < credits.size()) {
            return credits.get(index);
        }
        return null;
    }

}
