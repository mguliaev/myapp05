package com.example.misaki.myapp05.model.data.response;

public class ResponseMock {
    public static final String responseOrg = "{\n" +
            "  \"resultCode\": 200,\n" +
            "  \"error\": \"\",\n" +
            "  \"result\": {\n" +
            "    \"accounts\": [\n" +
            "      {\n" +
            "        \"name\": \"Счет До Востребования\",\n" +
            "        \"contractNumber\": 123453,\n" +
            "        \"openedDate\": \"2018-03-31T11:30:46+04:00\",\n" +
            "        \"percent\": 0.01,\n" +
            "        \"amount\": 218435.75,\n" +
            "        \"currency\": \"rur\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Счет Доходный\",\n" +
            "        \"contractNumber\": 489282,\n" +
            "        \"openedDate\": \"2015-03-21T13:58:57+00:00\",\n" +
            "        \"percent\": 0.5,\n" +
            "        \"amount\": 2400,\n" +
            "        \"currency\": \"rur\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Счет Быстрый\",\n" +
            "        \"contractNumber\": 489284,\n" +
            "        \"openedDate\": \"2015-03-21T13:58:57+00:00\",\n" +
            "        \"closingDate\": \"2017-06-19T11:18:17+00:00\",\n" +
            "        \"percent\": 0.1,\n" +
            "        \"status\":\"closed\",\n" +
            "        \"amount\": 24005,\n" +
            "        \"currency\": \"rur\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"deposits\": [\n" +
            "      {\n" +
            "        \"name\": \"Вклад Пенсионный\",\n" +
            "        \"contractNumber\": 459285,\n" +
            "        \"openedDate\": \"2018-03-21T13:58:57+00:00\",\n" +
            "        \"closingDate\": \"2019-03-21T13:58:57+00:00\",\n" +
            "        \"percent\": 5.3,\n" +
            "        \"amount\": 100000,\n" +
            "        \"currency\": \"rur\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Вклад Архивный\",\n" +
            "        \"contractNumber\": 2355755,\n" +
            "        \"openedDate\": \"2017-01-11T13:58:57+00:00\",\n" +
            "        \"closingDate\": \"2018-01-11T13:58:57+00:00\",\n" +
            "        \"percent\": 10,\n" +
            "        \"amount\": 200000,\n" +
            "        \"currency\": \"rur\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Вклад Валютный\",\n" +
            "        \"contractNumber\": 908777,\n" +
            "        \"openedDate\": \"2018-01-05T10:08:27+00:00\",\n" +
            "        \"closingDate\": \"2019-01-06T10:08:27+00:00\",\n" +
            "        \"percent\": 1.2,\n" +
            "        \"amount\": 10000,\n" +
            "        \"currency\": \"usd\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"creditCards\": [\n" +
            "      {\n" +
            "        \"name\": \"Кредитная карта Деньги\",\n" +
            "        \"contractNumber\": 443444,\n" +
            "        \"cardNumber\":\"4567xxxxxxxx0044\",\n" +
            "        \"status\": \"active\",\n" +
            "        \"openedDate\": \"2017-02-26T20:18:27+00:00\",\n" +
            "        \"overdueDate\": \"2018-06-21T10:50:46+00:00\",\n" +
            "        \"imageUrl\": \"http://www.papabankir.ru/wp-content/uploads/primer-nomera-kreditki.png\",\n" +
            "        \"amount\": -1356.15,\n" +
            "        \"currency\": \"rur\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Кредитная карта Золото\",\n" +
            "        \"contractNumber\": 443447,\n" +
            "        \"cardNumber\":\"5463xxxxxxxx0055\",\n" +
            "        \"status\":\"closed\",\n" +
            "        \"openedDate\": \"2015-02-26T20:18:27+00:00\",\n" +
            "        \"closingDate\": \"2017-09-09T21:58:57+00:00\",\n" +
            "        \"overdueDate\": \"2017-06-21T10:50:46+00:00\",\n" +
            "        \"imageUrl\": \"http://www.papabankir.ru/wp-content/uploads/primer-nomera-kreditki.png\",\n" +
            "        \"amount\": 4543.15,\n" +
            "        \"currency\": \"rur\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"debitCards\": [\n" +
            "      {\n" +
            "        \"name\": \"Дебетовая карта Кошелёк\",\n" +
            "        \"contractNumber\": 556767,\n" +
            "        \"cardNumber\":\"4568xxxxxxxx0044\",\n" +
            "        \"openedDate\": \"2017-09-10T12:25:27+00:00\",\n" +
            "        \"imageUrl\": \"https://fossbytes.com/wp-content/uploads/2016/12/credit-card-visa-hack.jpg\",\n" +
            "        \"amount\": 23000.90,\n" +
            "        \"currency\": \"rur\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Дебетовая карта Кошелёк\",\n" +
            "        \"contractNumber\": 2324421,\n" +
            "        \"cardNumber\":\"5555xxxxxxxx0044\",\n" +
            "        \"openedDate\": \"2017-09-10T12:25:27+00:00\",\n" +
            "        \"imageUrl\": \"https://fossbytes.com/wp-content/uploads/2016/12/credit-card-visa-hack.jpg\",\n" +
            "        \"amount\": 3000,\n" +
            "        \"currency\": \"usd\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"credits\": [\n" +
            "      {\n" +
            "        \"name\": \"Кредит Выгодный\",\n" +
            "        \"contractNumber\": 4666888,\n" +
            "        \"openedDate\": \"2018-02-09T21:58:57+00:00\",\n" +
            "        \"nextPaymentDate\": \"2018-05-11T05:58:57+00:00\",\n" +
            "        \"nextPaymentAmount\": 8900,\n" +
            "        \"percent\": 15.4,\n" +
            "        \"amount\": 125000,\n" +
            "        \"currency\": \"rur\",\n" +
            "        \"status\":\"active\",\n" +
            "        \"payments\": [\n" +
            "          {\n" +
            "            \"paymentDate\": \"2018-03-09T21:58:57+00:00\",\n" +
            "            \"paymentAmount\": 3400,\n" +
            "            \"status\": \"paid\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"paymentDate\": \"2018-04-09T21:58:57+00:00\",\n" +
            "            \"paymentAmount\": 3700,\n" +
            "            \"status\": \"paid\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"paymentDate\": \"2018-05-09T21:58:57+00:00\",\n" +
            "            \"paymentAmount\": 4400,\n" +
            "            \"status\": \"skipped\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"paymentDate\": \"2018-06-09T21:58:57+00:00\",\n" +
            "            \"paymentAmount\": 4400,\n" +
            "            \"status\": \"future\"\n" +
            "          }\n" +
            "        ]\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Кредит Нужный\",\n" +
            "        \"contractNumber\": 4666889,\n" +
            "        \"openedDate\": \"2017-02-09T21:58:57+00:00\",\n" +
            "        \"closingDate\": \"2017-08-09T21:58:57+00:00\",\n" +
            "        \"nextPaymentDate\": \"2017-05-11T05:58:57+00:00\",\n" +
            "        \"nextPaymentAmount\": 12000,\n" +
            "        \"percent\": 10.4,\n" +
            "        \"amount\": 12354000,\n" +
            "        \"currency\": \"rur\",\n" +
            "        \"status\":\"closed\",\n" +
            "        \"payments\": [\n" +
            "          {\n" +
            "            \"paymentDate\": \"2017-03-09T21:58:57+00:00\",\n" +
            "            \"paymentAmount\": 12300,\n" +
            "            \"status\": \"paid\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"paymentDate\": \"2017-05-09T21:58:57+00:00\",\n" +
            "            \"paymentAmount\": 34300,\n" +
            "            \"status\": \"skipped\"\n" +
            "          },\n" +
            "          {\n" +
            "            \"paymentDate\": \"2017-06-09T21:58:57+00:00\",\n" +
            "            \"paymentAmount\": 22200,\n" +
            "            \"status\": \"future\"\n" +
            "          }\n" +
            "        ]\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "}";
    static final String responseV01 = "{\n" +
            "  \"resultCode\": 200,\n" +
            "  \"error\": \"\"\n" +
            "}";
}
