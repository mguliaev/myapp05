package com.example.misaki.myapp05;

import com.example.misaki.myapp05.model.bankapi.BankApi;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        NetworkModule.class,
        UtilsModule.class,
        BuilderModule.class,
        RepositoryModule.class
})
public interface AppComponent {

    // предоставляет зависимость для зависимых компонентов
    // (не нужно для одно-компонентных приложений)
    BankApi provideBankApi();

    // позволяет инжектить зависимости в наш класс
    void inject(MainActivity mainActivity);
}

