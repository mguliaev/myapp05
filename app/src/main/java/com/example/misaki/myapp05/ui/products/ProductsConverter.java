package com.example.misaki.myapp05.ui.products;

import android.support.annotation.NonNull;

import com.example.misaki.myapp05.model.data.response.ProductsResponse;
import com.example.misaki.myapp05.model.products.Credit;
import com.example.misaki.myapp05.model.products.CreditCard;
import com.example.misaki.myapp05.model.products.DebitCard;
import com.example.misaki.myapp05.model.products.Deposit;

import java.util.ArrayList;
import java.util.List;

public class ProductsConverter {
    @NonNull
    public List<BaseProduct> convert(@NonNull ProductsResponse response) {
        List<BaseProduct> products = new ArrayList<>();
        List<BaseProduct> productsClosed = new ArrayList<>();
        if (response.getAccountSize() > 0) {
            //productTypes.add("Accounts");
        }
        if (response.getDepositsSize() > 0) {
            products.add(new ViewTitle("Deposits"));
            ArrayList<ViewDeposit> viewDepositsList = new ArrayList<>();
            ArrayList<ViewDeposit> viewDepositsClosedList = new ArrayList<>();
            for (int i = 0; i < response.getDepositsSize(); i++) {
                Deposit deposit = response.getDeposit(i);
                String s = deposit.toString();
                ViewDeposit viewDeposit = new ViewDeposit(s);
                if (deposit.getStatus() == null
                        || deposit.getStatus().equals("active")) {
                    viewDepositsList.add(viewDeposit);
                } else {
                    viewDepositsClosedList.add(viewDeposit);
                }
            }
            products.add(new ViewDeposits(viewDepositsList));
            if (viewDepositsClosedList.size() > 0) {
                productsClosed.add(new ViewDeposits(viewDepositsClosedList));
            }
        }
        if (response.getCreditsSize() > 0) {
            products.add(new ViewTitle("Credits"));
            ArrayList<ViewCredit> viewCreditsList = new ArrayList<>();
            ArrayList<ViewCredit> viewCreditsClosedList = new ArrayList<>();
            for (int i = 0; i < response.getCreditsSize(); i++) {
                Credit credit = response.getCredit(i);
                String s = credit.toString();
                ViewCredit viewCredit = new ViewCredit(s);
                if (credit.getStatus() == null
                        || credit.getStatus().equals("active")) {
                    viewCreditsList.add(viewCredit);
                } else {
                    viewCreditsClosedList.add(viewCredit);
                }
            }
            products.add(new ViewCredits(viewCreditsList));
            if (viewCreditsClosedList.size() > 0) {
                productsClosed.add(new ViewCredits(viewCreditsClosedList));
            }
        }
        if (response.getCreditCardsSize() > 0) {
            products.add(new ViewTitle("Credit Cards"));
            ArrayList<ViewCreditCard> viewCreditCardsList = new ArrayList<>();
            ArrayList<ViewCreditCard> viewCreditCardsClosedList = new ArrayList<>();
            for (int i = 0; i < response.getCreditCardsSize(); i++) {
                CreditCard creditCard = response.getCreditCard(i);
                String s = creditCard.toString();
                ViewCreditCard viewCreditCard = new ViewCreditCard(s);
                if (creditCard.getStatus() == null
                        || creditCard.getStatus().equals("active")) {
                    viewCreditCardsList.add(viewCreditCard);
                } else {
                    viewCreditCardsClosedList.add(viewCreditCard);
                }
            }
            products.add(new ViewCreditCards(viewCreditCardsList));
            if (viewCreditCardsClosedList.size() > 0) {
                productsClosed.add(new ViewCreditCards(viewCreditCardsClosedList));
            }
        }
        if (response.getDebitCardsSize() > 0) {
            products.add(new ViewTitle("Debit Cards"));
            ArrayList<ViewDebitCard> viewDebitCardsList = new ArrayList<>();
            ArrayList<ViewDebitCard> viewDebitCardsClosedList = new ArrayList<>();
            for (int i = 0; i < response.getDebitCardsSize(); i++) {
                DebitCard debitCard = response.getDebitCard(i);
                String s = debitCard.toString();
                ViewDebitCard viewDebitCard = new ViewDebitCard(s);
                if (debitCard.getStatus() == null
                        || debitCard.getStatus().equals("active")) {
                    viewDebitCardsList.add(viewDebitCard);
                } else {
                    viewDebitCardsClosedList.add(viewDebitCard);
                }
            }
            products.add(new ViewDebitCards(viewDebitCardsList));
            if (viewDebitCardsClosedList.size() > 0) {
                productsClosed.add(new ViewDebitCards(viewDebitCardsClosedList));
            }
        }
        if (productsClosed.size() > 0) {
            products.add(new ViewTitle("Closed"));
            products.addAll(productsClosed);
        }
        return products;
    }
}

