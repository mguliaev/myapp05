package com.example.misaki.myapp05.model.data.local;

import com.example.misaki.myapp05.ui.products.BaseProduct;

import java.util.List;

public class LocalRepository {
    private static final LocalRepository ourInstance = new LocalRepository();

    private List<BaseProduct> products;
    private Boolean isDirty = true;

    public static LocalRepository getInstance() {
        return ourInstance;
    }

    private LocalRepository() {
    }

    public List<BaseProduct> getProducts() {
        return products;
    }

    public void setProducts(List<BaseProduct> products) {
        this.products = products;
    }

    public Boolean getDirty() {
        return isDirty;
    }

    public void setDirty(Boolean dirty) {
        isDirty = dirty;
    }
}
