package com.example.misaki.myapp05.model.products;

public class ProductBuilder {
    private String name;
    private double percent;
    private double amount;
    private String currency;
    private double bonusPercent;
    private String status;

    public ProductBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ProductBuilder setPercent(double percent) {
        this.percent = percent;
        return this;
    }

    public ProductBuilder setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public ProductBuilder setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public ProductBuilder setBonusPercent(double bonusPercent) {
        this.bonusPercent = bonusPercent;
        return this;
    }

    public ProductBuilder setStatus(String status) {
        this.status = status;
        return this;
    }

    public Product createProduct() {
        return new Product(name, percent, amount, currency, bonusPercent, status);
    }
}