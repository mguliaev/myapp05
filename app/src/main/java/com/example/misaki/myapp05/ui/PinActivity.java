package com.example.misaki.myapp05.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.misaki.myapp05.MainActivity;
import com.example.misaki.myapp05.MyPreferences;
import com.example.misaki.myapp05.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PinActivity extends AppCompatActivity {

    @BindView(R.id.pin0Button)
    Button pin0Button;
    @BindView(R.id.pin1Button)
    Button pin1Button;
    @BindView(R.id.pin2Button)
    Button pin2Button;
    @BindView(R.id.pin3Button)
    Button pin3Button;
    @BindView(R.id.pin4Button)
    Button pin4Button;
    @BindView(R.id.pin5Button)
    Button pin5Button;
    @BindView(R.id.pin6Button)
    Button pin6Button;
    @BindView(R.id.pin7Button)
    Button pin7Button;
    @BindView(R.id.pin8Button)
    Button pin8Button;
    @BindView(R.id.pin9Button)
    Button pin9Button;
    //@BindView(R.id.pinBackButton)
    //Button pinBackButton;
    @BindView(R.id.pinBackImageView)
    ImageView pinBackImageView;
    @BindView(R.id.pinForgetButton)
    Button pinForgetButton;

    @BindView(R.id.pinTitleTextView)
    TextView pinTitleTextView;
    @BindView(R.id.pinValueTextView)
    TextView pinValueTextView;

    static final int PIN_LENGTH_DEF = 4;

    private String pinValue = "";

    public static final String PIN_KEY = "pin_saved";
    public static final String PIN_TIME_KEY = "pin_time_saved";

    PinStrategy pinStrategy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        ButterKnife.bind(this);
        Init();
        MyPreferences.saveText(this, PIN_TIME_KEY, "");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Init();
    }

    protected void Init() {
        String pinValueSaved = MyPreferences.loadText(this, PIN_KEY);
        if (pinValueSaved.length() == 0) {
            pinStrategy = new CreatePinStrategy();
        } else {
            pinStrategy = new InputPinStrategy();
        }
        applyPinStrategy(pinStrategy);
        pinValueShow();
    }

    @OnClick(R.id.pin0Button)
    public void onPin0ButtonClick(View v) {
        pinValueAdd("0");
    }

    @OnClick(R.id.pin1Button)
    public void onPin1ButtonClick(View v) {
        pinValueAdd("1");
    }

    @OnClick(R.id.pin2Button)
    public void onPin2ButtonClick(View v) {
        pinValueAdd("2");
    }

    @OnClick(R.id.pin3Button)
    public void onPin3ButtonClick(View v) {
        pinValueAdd("3");
    }

    @OnClick(R.id.pin4Button)
    public void onPin4ButtonClick(View v) {
        pinValueAdd("4");
    }

    @OnClick(R.id.pin5Button)
    public void onPin5ButtonClick(View v) {
        pinValueAdd("5");
    }

    @OnClick(R.id.pin6Button)
    public void onPin6ButtonClick(View v) {
        pinValueAdd("6");
    }

    @OnClick(R.id.pin7Button)
    public void onPin7ButtonClick(View v) {
        pinValueAdd("7");
    }

    @OnClick(R.id.pin8Button)
    public void onPin8ButtonClick(View v) {
        pinValueAdd("8");
    }

    @OnClick(R.id.pin9Button)
    public void onPin9ButtonClick(View v) {
        pinValueAdd("9");
    }

    //@OnClick(R.id.pinBackButton)
    @OnClick(R.id.pinBackImageView)
    public void onPinBackButtonClick(View v) {
        pinValueBack();
    }

    @OnClick(R.id.pinForgetButton)
    public void onPinForgetButtonClick(View v) {
        pinValueForget();
    }

    private void pinValueShow() {
        if (pinValue.length() > 0) {
            pinValueTextView.setText(pinValue);
        } else {
            pinValueTextView.setText(R.string.pin_value_def);
        }
    }

    private void pinValueAdd(String s) {
        if (pinValue.length() < PIN_LENGTH_DEF) {
            pinValue += s;
        }
        pinValueShow();
        if (pinValue.length() == PIN_LENGTH_DEF) {
            String pinValueSaved = MyPreferences.loadText(this, PIN_KEY);
            if (pinValueSaved.length() == 0) {
                MyPreferences.saveText(this, PIN_KEY, pinValue);
                pinStrategy.onComplete(PIN_KEY, pinValue, this, MainActivity.class);
            } else if (pinValueSaved.equals(pinValue)) {
                pinStrategy.onComplete(PIN_KEY, pinValue, this, MainActivity.class);
            } else {
                pinValue = "";
                applyPinStrategy(pinStrategy);
                pinTitleTextView.setText(R.string.pin_title_input_error);
            }
        }
    }

    private void pinValueBack() {
        if (pinValue.length() > 0) {
            pinValue = pinValue.substring(0, pinValue.length() - 1);
        }
        pinValueShow();
    }

    private void pinValueForget() {
        pinValue = "";
        MyPreferences.saveText(this, PIN_KEY, pinValue);
        pinValueShow();
        pinStrategy = new CreatePinStrategy();
        applyPinStrategy(pinStrategy);
    }

    void applyPinStrategy(PinStrategy strategy) {
        strategy.setTitle(pinTitleTextView);
        strategy.setForgetButton(pinForgetButton);
    }

}
