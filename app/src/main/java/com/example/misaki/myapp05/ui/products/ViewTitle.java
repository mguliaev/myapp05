package com.example.misaki.myapp05.ui.products;

public class ViewTitle extends BaseProduct {
    private String text = "<empty>";

    public ViewTitle(String text) {
        type = BaseProduct.TITLE;
        this.text = text;
    }

    String getText() {
        return text;
    }
}
