package com.example.misaki.myapp05.ui.products;

import java.util.ArrayList;

public class ViewDebitCards extends BaseProduct {

    protected ArrayList<ViewDebitCard> debitCards;

    public ViewDebitCards(ArrayList<ViewDebitCard> debitCards) {
        type = BaseProduct.DEBIT_CARD;
        this.debitCards = debitCards;
    }

    String getText() {
        String s;
        if(debitCards==null) {
            s = "<null>";
        } else {
            s = Integer.toString(debitCards.size());
        }
        return "ViewDebitCard: " + s;
    }

    int getSize() {
        if (debitCards == null) {
            return 0;
        }
        return debitCards.size();
    }

    ViewDebitCard getDebitCard(int index) {
        if (debitCards == null) {
            return null;
        }
        if (index >= 0 && index < debitCards.size()) {
            return debitCards.get(index);
        }
        return null;
    }

}
