package com.example.misaki.myapp05.model.data.remote;

import android.app.Service;
import android.widget.Button;
import android.widget.ProgressBar;

public class DownloadServiceParams {
    Service service;
    Integer numDef;
    Integer resultNumMaxDef;

    public DownloadServiceParams(Service service, Integer numDef, Integer resultNumMaxDef) {
        this.service = service;
        this.numDef = numDef;
        this.resultNumMaxDef = resultNumMaxDef;
    }

    public Service getService() {
        return service;
    }

    public Integer getNumDef() {
        return numDef;
    }

    public Integer getResultNumMaxDef() {
        return resultNumMaxDef;
    }
}
