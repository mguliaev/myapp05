package com.example.misaki.myapp05.ui.products;

import java.util.ArrayList;

public class ViewDeposits extends BaseProduct {

    protected ArrayList<ViewDeposit> deposits;

    public ViewDeposits(ArrayList<ViewDeposit> deposits) {
        type = BaseProduct.DEPOSIT;
        this.deposits = deposits;
    }

    String getText() {
        String s;
        if (deposits == null) {
            s = "<null>";
        } else {
            s = Integer.toString(deposits.size());
        }
        return "ViewDeposits: " + s;
    }

    int getSize() {
        if (deposits == null) {
            return 0;
        }
        return deposits.size();
    }

    ViewDeposit getDeposit(int index) {
        if (deposits == null) {
            return null;
        }
        if (index >= 0 && index < deposits.size()) {
            return deposits.get(index);
        }
        return null;
    }

}
