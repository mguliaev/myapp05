package com.example.misaki.myapp05.ui.products;

import android.view.View;

public class ViewCredit {
    private String text = "<empty>";

    public ViewCredit(String text) {
        this.text = text;
    }

    String getText() {
        return text;
    }
}
