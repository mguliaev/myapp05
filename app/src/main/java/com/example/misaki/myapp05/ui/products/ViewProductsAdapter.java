package com.example.misaki.myapp05.ui.products;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v7.widget.LinearLayoutManager;

import com.example.misaki.myapp05.R;

public class ViewProductsAdapter extends RecyclerView.Adapter<ViewProductsAdapter.ViewHolder> {
    ViewResponseObjects productItems;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private View productsView;

        public ViewHolder(View v) {
            super(v);
            productsView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ViewProductsAdapter(ViewResponseObjects products) {
        this.productItems = products;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v;
        Activity activity;
        switch (viewType) {
            case BaseProduct.TITLE:
                v = layoutInflater.inflate(R.layout.title_view, parent, false);
                break;
            case BaseProduct.DEPOSIT:
                v = layoutInflater.inflate(R.layout.deposit_view, parent, false);
                RecyclerView depositRecyclerView = v.findViewById(R.id.depositRecyclerView);
                activity = (Activity) depositRecyclerView.getContext();
                depositRecyclerView.setLayoutManager(
                        new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                break;
            case BaseProduct.CREDIT_CARD:
                v = layoutInflater.inflate(R.layout.credit_card_view, parent, false);
                RecyclerView creditCardRecyclerView = v.findViewById(R.id.creditCardRecyclerView);
                activity = (Activity) creditCardRecyclerView.getContext();
                creditCardRecyclerView.setLayoutManager(
                        new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                break;
            case BaseProduct.DEBIT_CARD:
                v = layoutInflater.inflate(R.layout.debit_card_view, parent, false);
                RecyclerView debitCardRecyclerView = v.findViewById(R.id.debitCardRecyclerView);
                activity = (Activity) debitCardRecyclerView.getContext();
                debitCardRecyclerView.setLayoutManager(
                        new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                break;
            case BaseProduct.CREDIT:
                v = layoutInflater.inflate(R.layout.credit_view, parent, false);
                RecyclerView creditRecyclerView = v.findViewById(R.id.creditRecyclerView);
                activity = (Activity) creditRecyclerView.getContext();
                creditRecyclerView.setLayoutManager(
                        new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                break;
            default:
                v = layoutInflater.inflate(R.layout.title_view, parent, false);

        }
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BaseProduct product = productItems.getProduct(position);
        if (product instanceof ViewTitle) {
            ViewTitle title = (ViewTitle) product;
            TextView titleView = holder.productsView.findViewById(R.id.pinTitleTextView);
            titleView.setText(title.getText());
        } else if (product instanceof ViewDeposits) {
            ViewDeposits deposits = (ViewDeposits) product;
            RecyclerView depositRecyclerView = holder.productsView.findViewById(R.id.depositRecyclerView);
            ViewDepositAdapter depositAdapter = new ViewDepositAdapter(deposits);
            depositRecyclerView.setAdapter(depositAdapter);
        } else if (product instanceof ViewCreditCards) {
            ViewCreditCards creditCards = (ViewCreditCards) product;
            RecyclerView creditCardRecyclerView = holder.productsView.findViewById(R.id.creditCardRecyclerView);
            ViewCreditCardAdapter creditCardAdapter = new ViewCreditCardAdapter(creditCards);
            creditCardRecyclerView.setAdapter(creditCardAdapter);
        } else if (product instanceof ViewDebitCards) {
            ViewDebitCards debitCards = (ViewDebitCards) product;
            RecyclerView debitCardRecyclerView = holder.productsView.findViewById(R.id.debitCardRecyclerView);
            ViewDebitCardAdapter debitCardAdapter = new ViewDebitCardAdapter(debitCards);
            debitCardRecyclerView.setAdapter(debitCardAdapter);
        } else if (product instanceof ViewCredits) {
            ViewCredits credits = (ViewCredits) product;
            RecyclerView creditRecyclerView = holder.productsView.findViewById(R.id.creditRecyclerView);
            ViewCreditAdapter creditAdapter = new ViewCreditAdapter(credits);
            creditRecyclerView.setAdapter(creditAdapter);
        } else {
            ViewTitle title = (ViewTitle) product;
            TextView titleView = holder.productsView.findViewById(R.id.pinTitleTextView);
            titleView.setText("ERROR TITLE");
        }
    }

    // Return the numDef of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return productItems.getProductTypesSize();
    }

    @Override
    public int getItemViewType(int position) {
        BaseProduct item = productItems.getProduct(position);
        if (item instanceof ViewTitle) {
            return BaseProduct.TITLE;
        } else if (item instanceof ViewDeposits) {
            return BaseProduct.DEPOSIT;
        } else if (item instanceof ViewCreditCards) {
            return BaseProduct.CREDIT_CARD;
        } else if (item instanceof ViewDebitCards) {
            return BaseProduct.DEBIT_CARD;
        } else if (item instanceof ViewCredits) {
            return BaseProduct.CREDIT;
        } else {
            return 0;
        }
    }

}

