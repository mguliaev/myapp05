package com.example.misaki.myapp05.model.products;

import com.google.gson.annotations.SerializedName;

public class CreditCard extends Product {
    @SerializedName("cardNumber")
    private String cardNumber;

    @Override
    public String toString() {
        String card_type = "";
        if (cardNumber != null && cardNumber.length() > 0) {
            if (cardNumber.charAt(0) == '4') {
                card_type = " VISA";
            } else if (cardNumber.charAt(0) == '5') {
                card_type = " MAESTRO";
            }
        }
        String statusOut = status;
        if (statusOut == null) {
            statusOut = "active";
        }
        return name
                + card_type
                + " " + Double.toString(percent) + "%"
                + " " + Double.toString(amount)
                + " " + currency
                + " " + statusOut;
    }

}
