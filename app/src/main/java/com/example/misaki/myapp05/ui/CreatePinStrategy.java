package com.example.misaki.myapp05.ui;

import android.widget.Button;
import android.widget.TextView;

import com.example.misaki.myapp05.R;

public class CreatePinStrategy extends PinStrategy {
    @Override
    public void setTitle(TextView v) {
        v.setText(R.string.pin_title_create);
    }

    @Override
    public void setForgetButton(Button v) {
        v.setText("");
    }

}
