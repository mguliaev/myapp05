package com.example.misaki.myapp05;

import com.example.misaki.myapp05.model.data.local.DepositesRepository;
import com.example.misaki.myapp05.model.data.local.LocalRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    DepositesRepository provideDepositesRepository() {
        return DepositesRepository.getInstance();
    }

    @Provides
    LocalRepository provideLocalRepository() {
        return LocalRepository.getInstance();
    }

}
